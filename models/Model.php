<?php
  include_once($_SERVER['DOCUMENT_ROOT'] . '/app/Configuration.php');

  class Model
  {
    protected $dbConnect;
    protected $tableName;

    function __construct() {
      $this->dbConnect = new PDO('mysql:host=' . Configuration::DB_HOST . ';dbname=' . Configuration::DB_NAME, Configuration::DB_USERNAME, Configuration::DB_PASSWORD);
    }

    public function getAll() {
      $request = $this->dbConnect->prepare('SELECT * FROM ' . $this->tableName);
      $request->execute();
      $results = $request->fetchAll(PDO::FETCH_OBJ);
      return $results;
    }
  }
 