<?php
  include('Model.php');
  include_once($_SERVER['DOCUMENT_ROOT'] . '/app/Configuration.php');

  class PageModel extends Model
  {
    function __construct() {
      $this->tableName = Configuration::TAB_PAGES;
      // Execute the parent constructor
      parent::__construct();
    }

    /**
     * Retrieve one specific page
     *
     * @param [int] $id
     * @return void
     */
    public function getOne($id) {
      $request = $this->dbConnect->prepare('SELECT * FROM ' . Configuration::TAB_PAGES . ' WHERE id =' . $id);
      $request->execute();
      $result = $request->fetchAll(PDO::FETCH_OBJ);
      if (count($result) == 0) {
        $result['error'] = '404';
        return $result;
      } else {
        return $result[0];
      }
    }

    public function updatePage($id, $title, $content) {
      $request = $this->dbConnect->prepare('UPDATE ' . Configuration::TAB_PAGES . ' SET title = "' . $title . '", content = "' . $content . '" WHERE id = ' . $id);
      $request->execute();
      $result = $request->fetchAll();
    }

  }
