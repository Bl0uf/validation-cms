<?php
  include_once('Model.php');
  include_once($_SERVER['DOCUMENT_ROOT'] . '/app/Configuration.php');

  class UserModel extends Model
  {
    function __construct() {
      $this->tableName = Configuration::TAB_USERS;
      parent::__construct();
    }

    public function logUser($username, $password) {
      $request = $this->dbConnect->prepare('SELECT username, is_connected FROM ' . Configuration::TAB_USERS . ' WHERE username = "' . $username . '" AND password = "' . $password . '"');
      $request->execute();
      $result = $request->fetchAll(PDO::FETCH_OBJ);
      if (count($result) == 0) {
        $result['error'] = '404';
        return $result;
      } else {
        // $requestUpdate = $this->dbConnect('UPDATE users SET is_connected = 1 WHERE username = "' . $username .'"');
        // $requestUpdate->execute();
        return $result[0];
      }
    }
  }
