<?php
  include_once($_SERVER['DOCUMENT_ROOT'] . '/app/Configuration.php');

  class TemplateHelper
  {
    public static function createTemplate($templateName, $contentFromDB) {
      $header = file_get_contents(Configuration::DIR_VIEWS . '/layouts/header.html');
      $footer = file_get_contents(Configuration::DIR_VIEWS . '/layouts/footer.html');
      if (!is_object($contentFromDB) && $contentFromDB['error'] == '404') {
        $template = file_get_contents(Configuration::DIR_VIEWS . '/default.html');
      } else {
        $template = file_get_contents(Configuration::DIR_VIEWS . '/' . $templateName . '.html');
      }
      $result = $header . $template . $footer;
      foreach ($contentFromDB as $key => $value) {
        if (strpos($result, '%%' . strtoupper($key) . '%%')) {
          $result = str_replace('%%' . strtoupper($key) . '%%', $value, $result);
        }
      }
      return $result;
    }

    public static function createList($templateName, $contentFromDB) {
      // var_dump($contentFromDB);die;
      $header = file_get_contents(Configuration::DIR_VIEWS . '/layouts/header.html');
      $footer = file_get_contents(Configuration::DIR_VIEWS . '/layouts/footer.html');
      $template = file_get_contents(Configuration::DIR_VIEWS . '/' . $templateName . '.html');
      $result = $header . $template . $footer;
      // echo '<pre>';
      // var_dump($contentFromDB);
      // echo '</pre>';
      // die;
      // $new = get_object_vars($contentFromDB);
      // var_dump($new );die;
      $i = 0;
      foreach ($contentFromDB[$i] as $key => $value) {
        echo '<pre>';
        var_dump(get_object_vars($contentFromDB[$i]));
        echo '</pre>';
        if ($key == 'title') {
          $i++;
          echo '<form action="/pages/redirect" method="POST">
          <input type="hidden" name="id" value="1">
          <input type="submit" value="' . $value . '"></form>';
          continue;
        }
      }
    }
  }
  