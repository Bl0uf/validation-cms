<?php

  class RouteHelper
  {
    const CLASS_SUFFIX = 'Controller';

    /**
     * Get the URI and format it
     *
     * @return void
     * @throws Exception
     */
    static public function getRoute() {
      $baseUri = $_SERVER['REQUEST_URI'];
      $baseUri = substr($baseUri, 1);
      $uris = explode('/', $baseUri);
      $uris[0] = $uris[0] . self::CLASS_SUFFIX;
      if (count($uris) !== 2) {
        throw new \Exception('Error');
      }
      self::executeAction($uris);
    }

    /**
     * @param $uris
     */
    static private function executeAction($uris) {
      ucfirst($uris[0])::{$uris[1]}();
    }
  }
