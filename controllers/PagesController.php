<?php

  class PagesController
  {
    protected $ID;

    /**
     * List all pages available
     *
     * @return void
     */
    public function listing() {
      /**
       * Récupérer toutes les pages
       */
      $page = new PageModel();
      echo TemplateHelper::createList('list', $page->getAll());
    }

    public function redirect() {
      $id = (int)$_POST['id'];
    }

    public function edit() {
      $page = new PageModel();
      echo TemplateHelper::createTemplate('edit', $page->getOne($id));
    }

    /**
     * Update the page after the submit
     *
     * @return void
     */
    public function update() {
      $pageModel = new PageModel();
      $query = $pageModel->updatePage($_POST['id'], $_POST['title'], $_POST['content']);
      echo 'Bien modifié, vous allez être redirigé dans quelques secondes vers la liste des pages.';
      header('refresh:2;url=/pages/listing');
    }
  }