<?php

  class AuthController
  {
    public function login() {
      echo TemplateHelper::createTemplate('login', new stdClass());
    }

    public function loginAction() {
      $user = new UserModel();
      $test = $user->logUser($_POST['username'], $_POST['password']);
      if (!is_object($test) && $test['error'] == '404') {
        echo 'Mauvais login et / ou mot de passe';
        header('refresh:2;url=/auth/login');
        die;
      } else {
        $_SESSION['username'] = $_POST['username'];
        // $_SESSION['is_connected'] = $test['is_connected'];
        echo 'C\'est bon';
      }
    }
  }
  