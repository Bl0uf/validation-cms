<?php
  session_start();
  spl_autoload_register(function($className) {
    $directory = '';
    if (strpos($className, 'Helper')) {
      $directory = 'helpers';
    } else if (strpos($className, 'Model')) {
      $directory = 'models';
    } else if (strpos($className, 'Controller')) {
      $directory = 'controllers';
    } else {
      throw new \Exception('error', 1);
    }
    include './' . $directory . '/' . $className . '.php';    
  });
  RouteHelper::getRoute();