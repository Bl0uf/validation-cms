<?php
return [
  'DATABASE' => [
    'HOST' => 'localhost',
    'USERNAME' => 'root',
    'PASSWORD' => 'root',
    'NAME' => 'cms'
  ],
  'URIS' => [
    'LOGIN' => '/auth/login'
  ],
  'DIRECTORIES' => [
    'VIEWS' => 'views',
    'LAYOUTS' => 'layouts'
  ]
];