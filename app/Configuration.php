<?php

  class Configuration
  {
    const DB_NAME = 'cms';
    const DB_HOST = 'localhost';
    const DB_USERNAME = 'root';
    const DB_PASSWORD = 'root';
    const URIS_LOGIN = '/auth/login';
    const DIR_VIEWS = 'views';
    const DIR_LAYOUTS = 'layouts';
    const TAB_USERS = 'users';
    const TAB_PAGES = 'pages';
  }